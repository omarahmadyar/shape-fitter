#include "shape.h"

// Shape
Shape::Shape(coord_t x, coord_t y, id_type id, bool asymmetric, std::initializer_list<coord_t> blanks)
   : _x(x), _y(y), _asymmetric(asymmetric), _id(id), _blanks(blanks)
{ }

Shape::const_iterator Shape::cbegin() const noexcept
{
   return *this;
}

Shape::const_iterator Shape::cend() const
{
   const_iterator end{*this, {_x, _y - 1}, static_cast<coord_t>(_x * _y)};
   ++(--end); //can't return here or else we break RVO because lval ref
   return end;
}

Shape::const_iterator Shape::begin() const noexcept
{
   return cbegin();
}

Shape::const_iterator Shape::end() const
{
   return cend();
}


// Shape Iter
Shape::const_iterator::const_iterator(Shape const &shape, pos_t pos, coord_t count)
   : _shape(shape), _pos(pos), _count(count)
{ }

Shape::const_iterator::const_iterator(const_iterator const &rhs) = default;

Shape::const_iterator& Shape::const_iterator::operator=(const_iterator const &rhs)
{
   if(&_shape != &rhs._shape) {
      throw std::runtime_error("Cannot copy assign iterators of different shapes");
   }
   _pos = rhs._pos;
   _count = rhs._count;
   return *this;

}

Shape::const_iterator& Shape::const_iterator::operator++()
{
   auto &blanks = _shape._blanks;
   do {
      ++_pos.first;
      if(_pos.first >= _shape._x)
      {
         _pos.first = 0;
         ++_pos.second;
      }
   } while(std::find(blanks.cbegin(), blanks.cend(), ++_count) != blanks.cend());
   return *this;
}

Shape::const_iterator& Shape::const_iterator::operator--()
{
  auto &blanks = _shape._blanks;
  do {
     --_pos.first;
     if(_pos.first < 0)
     {
        _pos.first = _shape._x - 1;
        --_pos.second;
     }
  } while(std::find(blanks.cbegin(), blanks.cend(), --_count) != blanks.cend());
  return *this;

}

bool Shape::const_iterator::operator!=(const_iterator const &rhs) const noexcept
{
   return &_shape != &rhs._shape or _pos != rhs._pos or _count != rhs._count;
}


// ShapeState
bool ShapeState::inc() noexcept
{
   if(_shape._asymmetric and _flipped == false)
   {
      _flipped = true;
      return true;
   }

   if(_angle < 3)
   {
      _flipped = false;
      ++_angle;
      return true;
   }

   if(_xidx < _xSize - 1)
   {
      _flipped = false;
      _angle = 0;
      ++_xidx;
      return true;
   }

   if(_yidx < _ySize - 1)
   {
      _flipped = false;
      _angle = 0;
      _xidx = 0;
      ++_yidx;
      return true;
   }

   return false;
}

void ShapeState::reset() noexcept
{
   _flipped = false;
   _angle = 0;
   _xidx = 0;
   _yidx = 0;
}

ShapeState::const_iterator ShapeState::cbegin() const noexcept
{
   return *this;
}

ShapeState::const_iterator ShapeState::cend() const
{
   const_iterator end{*this};
   end._iter = _shape.cend();
   end.eval();
   return end;
}

ShapeState::const_iterator ShapeState::begin() const noexcept
{
   return cbegin();
}

ShapeState::const_iterator ShapeState::end() const
{
   return cend();
}


// ShapeState const_iterator
ShapeState::const_iterator::const_iterator(ShapeState const &shape) noexcept
   : _xbeg(shape._xidx), _ybeg(shape._yidx), _angle(shape._angle), _flipped(shape._flipped), _iter(shape._shape.cbegin())

{
   eval();
}

void ShapeState::const_iterator::eval() noexcept
{
   coord_t xoff{_iter->first},
           yoff{_iter->second};
   switch(_angle)
   {
      case 0:
         break;
      case 1:
         std::swap(xoff, yoff);
         xoff *= -1;
         break;
      case 2:
         xoff *= -1;
         yoff *= -1;
         break;
      case 3:
         std::swap(xoff, yoff);
         yoff *= -1;
         break;
      default:
         // shouldn't be possible
         break;
   }

   if(_flipped)
      xoff *= -1;

   _pos.first  = _xbeg + xoff;
   _pos.second = _ybeg + yoff;
}

ShapeState::const_iterator& ShapeState::const_iterator::operator++()
{
   ++_iter;
   eval();
   return *this;
}

ShapeState::const_iterator& ShapeState::const_iterator::operator--()
{
   --_iter;
   eval();
   return *this;
}

ShapeState::const_iterator ShapeState::const_iterator::operator--(int)
{
   auto copy = *this;
   --(*this);
   return copy;
}

