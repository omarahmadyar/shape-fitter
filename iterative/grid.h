#ifndef __GRID_H__
#define __GRID_H__

#include "shape.h"
#include "rect.h"

#include <iostream>

#include <vector>
#include <unordered_map>
#include <list>

#include <utility>

#include <cinttypes>
#include <cstring>



// Grid Class --------------------------------------------------------
template<coord_t _X, coord_t _Y>
class Grid
{
private:
   id_type _grid[_X][_Y];

public:
   static std::unordered_map<id_type, std::string> COLOR_;
   static char const _BLANK = '0';

   Grid();
   void clear(uint8_t x = _BLANK);
   bool placeShape(ShapeState const &shpst, id_type replace = _BLANK, id_type val = 0);
   void clearShape(ShapeState const &shpst);
   const auto & get() { return _grid; }

   template<coord_t X_, coord_t Y_>
   friend std::ostream& operator<<(std::ostream &,  Grid<X_,Y_> const &);
};

template<coord_t _X, coord_t _Y>
std::unordered_map<id_type, std::string> Grid<_X,_Y>::COLOR_{};

#include "grid.tcc" //include template implementations
#endif

