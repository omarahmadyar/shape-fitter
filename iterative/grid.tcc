#include <stdexcept>

// Grid ==============================================================
template<coord_t _X, coord_t _Y>
Grid<_X,_Y>::Grid()
{
   clear();
}


template<coord_t _X, coord_t _Y>
void Grid<_X,_Y>::clear(uint8_t x)
{
   for(coord_t i = 0; i < _X; ++i)
      memset(_grid[i], x, sizeof(uint8_t) * _Y);
}


template<coord_t _X, coord_t _Y>
bool Grid<_X,_Y>::placeShape(ShapeState const &shpst, id_type replace, id_type val)
{
   if(auto last = --shpst.cend(); last->first  < 0 or last->first  >= _X or
                                  last->second < 0 or last->second >= _Y)
      return false;

   if(val == 0) val = shpst._shape._id;
   for(auto iter = shpst.cbegin(); iter != shpst.cend(); ++iter)
   {
      if(iter->first  < 0 or iter->first  >= _X or
         iter->second < 0 or iter->second >= _Y or
         _grid[iter->first][iter->second] != replace)
      {
         while(iter-- != shpst.cbegin())
            _grid[iter->first][iter->second] = replace;
         return false;
      }

      _grid[iter->first][iter->second] = val;
   }
   return true;
}


template<coord_t _X, coord_t _Y>
void Grid<_X,_Y>::clearShape(ShapeState const &shpst)
{
   if(not placeShape(shpst, shpst._shape._id, _BLANK))
      throw std::runtime_error("Failed to Clear Shape");
   /* if it can't clear the shape that means
      there is a significant mistake somewhere */
}


template<coord_t _X, coord_t _Y>
std::ostream& operator << (std::ostream &out, Grid<_X,_Y> const &grid)
{
   static char const * RESET = "\x1b[0m"; // sequence to reset terminal foreground color
   for(coord_t y = 0; y < _Y; ++y)
   {
      for(coord_t x = 0; x < _X; ++x)
      {
         auto color_iter = grid.COLOR_.find(grid._grid[x][y]);
         out << (color_iter == grid.COLOR_.cend() ? RESET : color_iter->second);
         out << static_cast<char>(grid._grid[x][y]) << ' ';
         out << RESET;
      }
      out << '\n';
   }
   return out << RESET;
}

