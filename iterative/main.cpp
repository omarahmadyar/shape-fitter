#include "grid.h"
#include "shape.h"

#include <iostream>
#include <iomanip>
#include <chrono>

template<coord_t _X, coord_t _Y>
bool solve_board(Grid<_X,_Y> &board, std::vector<ShapeState> shapes);

using namespace std::chrono;
const auto INTERVAL = 1000ms;

int main()
{
   // Create Board
   Grid<8,8> board;

   // Setup shapes
   Shape orange = {4, 2, 'o', false, {}};
   Shape green  = {4, 3, 'g', false, {1,2,5,6}};
   Shape brown  = {3, 4, 'b', true, {2,5,6,9}};
   Shape cyan   = {4, 3, 'c', false, {4,7,8,11}};
   Shape dred   = {3, 3, 'd', true, {6}};
   Shape red    = {2, 5, 'r', true, {7,9}};
   Shape yellow = {4, 3, 'y', false, {2,3,10,11}};
   Shape purple = {4, 3, 'p', true, {4,5,8,9}};

   // Setup colors
   bool fg = true, //foreground
        bg = true; //background
   if(fg)
   {
      Grid<8,8>::COLOR_[orange._id] += "\x1b[37m";  //ANSI white
      Grid<8,8>::COLOR_[cyan._id]   += "\x1b[34m";  //ANSI blue
      Grid<8,8>::COLOR_[red._id]    += "\x1b[96m";  //ANSI cyan
      Grid<8,8>::COLOR_[yellow._id] += "\x1b[33m";  //ANSI yellow
      Grid<8,8>::COLOR_[purple._id] += "\x1b[35m";  //ANSI magenta
      Grid<8,8>::COLOR_[dred._id]   += "\x1b[31m";  //ANSI red
      Grid<8,8>::COLOR_[brown._id]  += "\x1b[30m";  //ANSI black
      Grid<8,8>::COLOR_[green._id]  += "\x1b[32m";  //ANSI green

   }
   if(bg)
   {
      Grid<8,8>::COLOR_[orange._id] += "\x1b[47m";  //ANSI white
      Grid<8,8>::COLOR_[cyan._id]   += "\x1b[44m";  //ANSI blue
      Grid<8,8>::COLOR_[red._id]    += "\x1b[106m"; //ANSI cyan
      Grid<8,8>::COLOR_[yellow._id] += "\x1b[103m"; //ANSI yellow
      Grid<8,8>::COLOR_[purple._id] += "\x1b[45m";  //ANSI magenta
      Grid<8,8>::COLOR_[dred._id]   += "\x1b[41m";  //ANSI red
      Grid<8,8>::COLOR_[brown._id]  += "\x1b[40m";  //ANSI black
      Grid<8,8>::COLOR_[green._id]  += "\x1b[42m";  //ANSI green
   }

   std::vector<ShapeState> shapes = { orange
                                    , green
                                    , brown
                                    , cyan
                                    , dred
                                    , red
                                    , yellow
                                    , purple
                                    };

   // Shuffle
   using namespace std::chrono;
//   srand(duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count());
//   std::random_shuffle(shapes.begin(), shapes.end());

   // Time Run
   auto begin = high_resolution_clock::now();
   bool result = solve_board(board, shapes);
   auto time = high_resolution_clock::now() - begin;

   // Print Solution
   if(result)
      std::cout << "SOLUTION:\n" << board << std::endl;
   else std::cout << "NO SOLUTION\n";

   // Print Time
   auto min = duration_cast<minutes>(time);
   double sec = duration_cast<milliseconds>(time - min).count() / 1000.0f;
   if(min.count() > 0) std::cout << min.count() << " min ";
   std::cout << sec << " sec\n";

   // Print time for stats
   std::cout << "REGEX_MS:";
   std::cout << std::setw(10) << std::setfill('0');
   std::cout << duration_cast<milliseconds>(time).count() << std::endl;


}

// End Main ==========================================================

template<coord_t _X, coord_t _Y>
bool solve_board(Grid<_X,_Y> &board, std::vector<ShapeState> shapes)
{
   // Shape Areas -- Optimization Setup -- if smallest area is smaller than smallest shape
   std::vector<unsigned> areas;
   areas.reserve(shapes.size());
   for(auto const &shape : shapes)
      areas.push_back(shape._shape._x * shape._shape._y - shape._shape._blanks.size());

   auto good = [&areas, &board](std::size_t count) {
      auto smallest = nonRectArea(board.get());
      for(auto c = count; c < areas.size(); ++c)
         if(smallest < areas[c]) return false;
      return true;
   };

   // Start Time
   auto time = std::chrono::steady_clock::now() - INTERVAL;

   // Loop over shapes
   std::vector<bool> firstRun(shapes.size(), true);
   for(decltype(shapes.size()) count = 0; count < shapes.size(); )
   {
      // Print Timing
      if(std::chrono::steady_clock::now() - time > INTERVAL)
      {
         std::cout << count << std::endl;
         std::cout << board << std::endl;
         time = std::chrono::steady_clock::now();
      }

      // Select Shape and increment if you back into it
      auto &state = shapes[count];
      bool bad = false;
      if(not firstRun[count]) {
         board.clearShape(state);
         bad = not state.inc();
      } else firstRun[count] = false;

      // Optimization
      bad = bad or not good(count);

      // Attempt to place the shape into any pos w/ any angle
      bool success = true;
      if(not bad)
      {
         while(not (success = board.placeShape(state)))
         {
            if(not state.inc())
            {
               if(count == 0) return false; // Failure
               else break;
            }
         }
      } else success = false;

      // Impossible To Place In Current Board
      if(not success)
      {
         if(count == 0) return false;
         state.reset();
         firstRun[count] = true;
         --count;
      }
      else ++count;
   }

   // Success
   return true;
}

