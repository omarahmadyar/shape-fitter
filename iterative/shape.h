#ifndef __SHAPES_H__
#define __SHAPES_H__

#include <vector>
#include <initializer_list>
#include <utility>
#include <cinttypes>
#include <stdexcept>

using coord_t = int8_t;
using pos_t = std::pair<coord_t, coord_t>;
using id_type = uint8_t;
using blanks_t = std::vector<coord_t>;

// Shape -------------------------------------------------------------
struct Shape
{
   coord_t const _x{-1};
   coord_t const _y{-1};
   bool const _asymmetric:1 = false;
   id_type const _id:7 = '0';
   blanks_t const _blanks;

   Shape( coord_t x
        , coord_t y
        , id_type id = '0'
        , bool asymmetric = false
        , std::initializer_list<coord_t> blanks = {}
        );


   class const_iterator;
   const_iterator cbegin() const noexcept;
   const_iterator cend() const;
   const_iterator begin() const noexcept;
   const_iterator end() const;
};

// Shape iterator
class Shape::const_iterator
{
   Shape const &_shape;
   pos_t _pos{0,0};
   coord_t _count{0};

   const_iterator(Shape const &shape, pos_t pos = {0,0}, coord_t count = 0);

public:
   const_iterator(const_iterator const &rhs);
   const_iterator& operator=(const_iterator const &rhs);
   pos_t const &operator*()  const noexcept { return  _pos; }
   pos_t const *operator->() const noexcept { return &_pos; }
   const_iterator& operator++();
   const_iterator& operator--();
   bool operator!=(const_iterator const &rhs) const noexcept;

   friend class Shape;
};


// ShapeState --------------------------------------------------------
struct ShapeState
{
   Shape const _shape;
   coord_t const _xSize = 8;
   coord_t const _ySize = 8;
   coord_t _xidx;
   coord_t _yidx;
   uint8_t _angle:4;
   bool _flipped:1;


   template<typename Shape_T>
   ShapeState( Shape_T &&shape
             , coord_t xidx = 0
             , coord_t yidx = 0
             , uint8_t angle = 0
             , bool flipped = false
             ) : _shape(std::forward<Shape_T>(shape))
               , _xidx(xidx), _yidx(yidx), _angle(angle), _flipped(flipped)
   { }

   bool inc() noexcept;
   void reset() noexcept;

   class const_iterator;
   const_iterator cbegin() const noexcept;
   const_iterator cend()   const;
   const_iterator begin() const noexcept;
   const_iterator end()   const;
};

// ShapeState iterator
class ShapeState::const_iterator
{
   coord_t const _xbeg;
   coord_t const _ybeg;
   uint8_t const _angle:4;
   bool  const _flipped:1;
   Shape::const_iterator _iter;
   pos_t _pos = {-1,-1};

   const_iterator(ShapeState const &shape) noexcept;
   void eval() noexcept;

public:
   pos_t const &operator*()  const noexcept { return  _pos; }
   pos_t const *operator->() const noexcept { return &_pos; }
   const_iterator& operator++();
   const_iterator& operator--();
   const_iterator operator--(int);
   bool operator!=(const_iterator const &rhs) const noexcept {
      return _iter != rhs._iter;
   }

   friend class ShapeState;
};

#endif

