#ifndef __RECT_H__
#define __RECT_H__

#include <queue>
#include <unordered_set>

template<typename T1, typename T2>
struct PairHash {
   std::size_t operator () (std::pair<T1,T2> const & x) const noexcept {
      return std::hash<T1>{}(x.first) ^ (std::hash<T2>{}(x.second)<<1);
   }
};

using pos_hash = PairHash<coord_t, coord_t>;
using unset = std::unordered_set<pos_t, pos_hash>;

template<typename T, coord_t _X, coord_t _Y>
unset nonRectAreaHelper(T const (&arr)[_X][_Y], pos_t const &pos, T search)
{
   /*
      Helper function that performs a breadth first expansion
   */
   unset explored;
   std::queue<pos_t> bfs;
   if(arr[pos.first][pos.second] == search) {
      bfs.push(pos);
      explored.insert(pos);
   }

   // BFS outwards exploring all adjacent locations (non diagonal)
   while(not bfs.empty())
   {
      auto const & node = bfs.front();
      for(int8_t off = -1; off <= 1; off += 2)
      {
         if(node.first + off >= 0 and node.first + off < _X
            and arr[node.first+off][node.second] == search and
            not explored.contains(pos_t{node.first+off,node.second}))
         {
            bfs.push(pos_t{node.first+off,node.second});
            explored.insert(pos_t{node.first+off,node.second});
         }
         if(node.second + off >= 0 and node.second + off < _Y
            and arr[node.first][node.second+off] == search and
            not explored.contains(pos_t{node.first,node.second+off}))
         {
            bfs.push(pos_t{node.first,node.second+off});
            explored.insert(pos_t{node.first,node.second+off});
         }

      }
      bfs.pop();
   }

   return explored;
}

/*
   Returns the target contiguous area of a 2d array

   Target: Depends on the provided comparator (default = smallest)
   Search: Whatever you are looking for (default = 0)

   Default: Will return the smallest contiguous area of 0's
   NOTE: Any continuous chain or area of search, not just rectangular.
   0 1 1 <-- will yield a result of 6     0 1 0
   0 1 0     will yield a result of 2 --> 0 1 0
   0 0 0                                  0 0 1

   Change behavior from smallest to biggest by changing OP to std::greater<T>
   Change search behavior by changing search from 0 to whatever
*/
template<typename T, typename OP = std::less<std::size_t>, coord_t _X, coord_t _Y>
std::size_t nonRectArea(T const (&arr)[_X][_Y], T search = '0', OP const &op = OP())
{
   unset undiscovered;
   for(coord_t x = 0; x < _X; ++x)
      for(coord_t y = 0; y < _Y; ++y)
         undiscovered.insert(pos_t{x,y});

   // Run Until All Nodes Have Been Discovered
   std::size_t target{};
   bool init{false};
   decltype(undiscovered.cbegin()) iter;
   while((iter = undiscovered.cbegin()) != undiscovered.cend())
   {
      auto explored = nonRectAreaHelper(arr, *iter, search);
      undiscovered.erase(iter);
      if(explored.size() == 0) continue;
      if(not init or op(explored.size(), target))
         target = explored.size();
      for(auto const & pos : explored)
         undiscovered.erase(pos);
      init = true;
   }
   return target;
}

#endif

