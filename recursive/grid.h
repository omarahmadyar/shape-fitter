#ifndef __GRID_H__
#define __GRID_H__
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <list>

#include <utility>

#include <cinttypes>
#include <cstring>

#include "types.h"

template<typename T1, typename T2>
struct PairHash
{
   std::size_t operator () (std::pair<T1,T2> const & x) const {
      return std::hash<coord_type>{}(x.first) ^ (std::hash<coord_type>{}(x.second)<<1);
   }
};


// Shape structures --------------------------------------------------
struct Shape
{
   coord_type _x{};
   coord_type _y{};
   uint8_t _id{ID_COUNTER++};
   unset _blanks;

   static uint8_t ID_COUNTER;

   Shape(coord_type x, coord_type y, unset blanks = {}) : _x(x), _y(y), _blanks(blanks)
   {
      if(_blanks.contains({0,0}))
         throw std::runtime_error("Shape::Shape: blanks may not contain {0,0}");
   }
};

uint8_t Shape::ID_COUNTER = 1;

struct OShape : Shape
{
   uint8_t _orientation:2 = {0};
   bool _flipped:1 = {false};
};


// Grid Class --------------------------------------------------------
template<coord_type _X, coord_type _Y>
class Grid
{
private:
   shape_id _grid[_X][_Y];

public:
   static std::unordered_map<shape_id, std::string> COLOR_;

   Grid();
   void clear(uint8_t x = 0);
   bool makeFit(std::list<Shape>);
   bool possibleFit(OShape);
   bool tryFit(OShape const &, coords, bool writeout = true);

   template<coord_type X_, coord_type Y_>
   friend std::ostream& operator<<(std::ostream &,  Grid<X_,Y_> const &);
};

template<coord_type _X, coord_type _Y>
std::unordered_map<shape_id, std::string> Grid<_X,_Y>::COLOR_{};

#include "grid.tcc" //include template implementations
#endif

