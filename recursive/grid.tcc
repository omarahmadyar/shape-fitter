#include "rect.h"

#include <climits>
#include <chrono>

std::chrono::steady_clock::duration PRINT_FREQ = std::chrono::seconds(1);
// Grid ==============================================================
template<coord_type _X, coord_type _Y>
Grid<_X,_Y>::Grid()
{
   clear();
}


template<coord_type _X, coord_type _Y>
void Grid<_X,_Y>::clear(uint8_t x)
{
   for(coord_type i = 0; i < _X; ++i)
      memset(_grid[i], x, sizeof(uint8_t) * _Y);
}


template<coord_type _X, coord_type _Y>
bool Grid<_X,_Y>::makeFit(std::list<Shape> shapes)
{
   /*
      Will attempt to insert every shape in shapes to *this
      Return whether or not it was successful
      If not successful, any modifications are undone.
   */


   if(shapes.empty()) return true;

   // Find Permissable number of lost squares -- IF perfect fit THEN 0
      /* Could be hard coded to save time but this way it works
         even if the grid and/or shapes are changed */
   unsigned allowed_lost_area = 0;

   for(coord_type x = 0; x < _X; ++x)
      for(coord_type y = 0; y < _Y; ++y)
         if(_grid[x][y] == 0) ++allowed_lost_area;

   unsigned smallest_shape_area = shapes.size()==0 ? 0 : INT_MAX;
   for(auto const & shape : shapes)
   {
      auto shape_area = shape._x * shape._y - shape._blanks.size();
      allowed_lost_area -= shape_area;
      if(shape_area < smallest_shape_area) smallest_shape_area = shape_area;
   }

   // Do some testing to see if this state is bad

   // Area cut off from the rest is lost if it has less area than every shape
   // Bad if we lost more area than acceptable
   unsigned smallest_area = nonRectArea(this->_grid);
   if(smallest_area + allowed_lost_area < smallest_shape_area)
      return false;

   // If any shape cannot be inserted into this state then it is bad
   for(auto const &shape : shapes)
      if(not possibleFit(OShape{shape}))
         return false;

   /* Passed previous tests, doesn't mean it's possible to find a solution
      but it prevents going into really obvious bad rabbit holes */
   using namespace std::chrono;
   static auto last_print = steady_clock::now() - PRINT_FREQ;
   if(steady_clock::now() > last_print + PRINT_FREQ)
   {
      std::cout << *this << std::endl;
      last_print = steady_clock::now();
   }

   // Begin Actual Function
   bool symmetrical = false;
   auto backup_state = *this;

   // Remove The Current Shape We Are Working on From Shapes
   OShape osh{std::move(shapes.front())};
   shapes.pop_front();

   // Symmetry Check
   bool xsym = true;
   bool ysym = true;
   for(auto const &blank : osh._blanks)
   {
      if(not osh._blanks.contains({osh._x - blank.first - 1, blank.second}))
         xsym = false;
      if(not osh._blanks.contains({blank.first, osh._y - blank.second - 1}))
         ysym = false;
   }
   symmetrical = xsym || ysym;

   // Place head shape in every possible location and buffer each instance
   std::vector<Grid> positions;
   positions.reserve(_Y * _X * 4 * (symmetrical?1:2));
   for(coord_type x = 0; x < _X; ++x)
   {
      for(coord_type y = 0; y < _Y; ++y)
      {
         if(_grid[x][y] != 0) continue;
         for(uint8_t rot = 0; rot < 4; ++rot)
         {
            for(uint8_t flip = 0; flip <= 1; ++flip)
            {
               osh._orientation = rot;
               osh._flipped = static_cast<bool>(flip);
               if(tryFit(osh, {x,y}))
               {
                  positions.push_back(*this);
                  *this = backup_state;
               }

               if(symmetrical) break;
            }
         }
      }
   }

   //// Prioritize instances with the greatest rect area of free space
   ///* Doing this is bad in many circumstances */
   //auto comp = [](auto const &x, auto const &y) {
   //   return biggestArea(x._grid) > biggestArea(y._grid);
   //};
   //std::stable_sort(positions.begin(), positions.end(), comp);

   // Go over every scenario and recurse
   for(auto iter = positions.cbegin(); iter != positions.cend(); ++iter)
   {
      *this = *iter;
      if(makeFit(shapes))
         return true;
   }
   *this = backup_state;
   return false;
}


template<coord_type _X, coord_type _Y>
bool Grid<_X,_Y>::possibleFit(OShape oshape)
{
   /*
      See if it is possible to insert oshape
      DOESN'T ACTUALL WRITE ANYTHING
   */

   for(coord_type x = 0; x < _X; ++x)
   {
      for(coord_type y = 0; y < _Y; ++y)
      {
         if(_grid[x][y] != 0) continue;
         for(int8_t rot = 0; rot < 4; ++rot)
         {
            oshape._orientation = rot;
            for(int8_t flip = 0; flip <= 1; ++flip)
            {
               oshape._flipped = static_cast<bool>(flip);
               if(tryFit(oshape, {x,y}, false)) return true;
            }
         }
      }
   }
   return false;
}


template<coord_type _X, coord_type _Y>
bool Grid<_X,_Y>::tryFit(OShape const &oshape, coords pos, bool writeout)
{
   /*
      Return whether it is possible to insert the OShape
      in its current state.

      If writeout is false, then the shape is not actually
      written to the grid. (writeout is true by default)
   */

   if(pos.first >= _X or pos.second >= _Y)
      throw std::out_of_range("Grid::tryFit: pos");
   if(_grid[pos.first][pos.second] != 0) return false;

   // Determine Case
   coord_type xinc = 1,
              yinc = 1,
              xtarg = oshape._x,
              ytarg = oshape._y;
   unset blanks(oshape._blanks.bucket_count());

   if(oshape._orientation == 0) { // 0 degrees
      blanks = oshape._blanks;
   } else if(oshape._orientation == 1) { // 90 degrees counterclockwise
      std::swap(xtarg, ytarg);
      xtarg *= -1; xinc = -1;
      for(auto const &entry : oshape._blanks)
         blanks.insert({-1 * entry.second, entry.first});
   } else if(oshape._orientation == 2) { // 180 deg counter
      xtarg *= -1; xinc = -1;
      ytarg *= -1; yinc = -1;
      for(auto const &entry : oshape._blanks)
         blanks.insert({-1 * entry.first, -1 * entry.second});
   } else if(oshape._orientation == 3) { // 270 deg counter
      std::swap(xtarg, ytarg);
      ytarg *= -1; yinc = -1;
      for(auto const &entry : oshape._blanks)
         blanks.insert({entry.second, -1 * entry.first});
   }

   if(oshape._flipped) { // flip along y axis
      xtarg *= -1; xinc *= -1;
      auto temp = blanks;
      blanks.clear();
      for(auto const &entry : temp)
         blanks.insert({-1 * entry.first, entry.second});
   }

   // Bounds Check
   if(pos.first + xtarg > _X     or pos.second + ytarg > _Y or
      pos.first + xtarg + 1 <  0 or pos.second + ytarg + 1 <  0)
      return false;

   // Changes written to temporary first before being finalized
   std::vector<coords> pending;
   if(writeout) pending.reserve(oshape._x * oshape._y - oshape._blanks.size());

   // Try Fit
   for(coord_type x = 0; x != xtarg; x += xinc)
   {
      for(coord_type y = 0; y != ytarg; y += yinc)
      {
         if(blanks.contains({x,y})) continue;
         else if(_grid[pos.first + x][pos.second + y] != 0) return false;
         else if(writeout) pending.push_back({pos.first + x, pos.second + y});
      }
   }

   // Write Out Changes
   for(const auto &loc : pending)
      _grid[loc.first][loc.second] = oshape._id;
   return true;
}


template<coord_type _X, coord_type _Y>
std::ostream& operator << (std::ostream &out, Grid<_X,_Y> const &grid)
{
   static char const * RESET = "\x1b[0m"; // sequence to reset terminal foreground color
   for(coord_type y = _Y - 1; y >= 0; --y)
   {
      for(coord_type x = 0; x < _X; ++x)
      {
         auto color_iter = grid.COLOR_.find(grid._grid[x][y]);
         out << (color_iter == grid.COLOR_.cend() ? RESET : color_iter->second);
         out << static_cast<int>(grid._grid[x][y]) << ' ';
         out << RESET;
      }
      out << '\n';
   }
   return out << RESET;
}

