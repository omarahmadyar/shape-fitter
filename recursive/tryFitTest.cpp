#include <iostream>
#include <cassert>
#define private public
#include "grid.h"

using namespace std;
int main()
{
   Grid<8,8> grid;
   OShape osh({4, 3, {{1,1}, {1,2}, {2,1}, {2,2}, {3,2}, {3,1}}});

   // Test 1
   cout << "Test 1 ---------------------------------\n";
   assert(grid.tryFit(osh, {0,0}));
   cout << grid << endl;

   // Test 2
   cout << "Test 2 ---------------------------------\n";
   grid.clear(9);
   grid._grid[0][0] = 0;
   grid._grid[0][1] = 0;
   grid._grid[0][2] = 0;

   grid._grid[1][0] = 0;
   grid._grid[2][0] = 0;

   grid._grid[3][0] = 0;
   assert(grid.tryFit(osh, {0,0}));
   cout << grid << endl;

   // Test 3
   cout << "Test 3 ---------------------------------\n";
   grid.clear(9);

   grid._grid[2][7] = 0;
   grid._grid[2][6] = 0;
   grid._grid[2][5] = 0;
   grid._grid[2][4] = 0;

   grid._grid[0][4] = 0;
   grid._grid[1][4] = 0;
   osh._orientation = 1;
   assert(grid.tryFit(osh, {2, 4}));
   cout << grid << endl;

   // Test 4
   cout << "Test 4 ---------------------------------\n";
   grid.clear(9);
   osh._orientation = 2;

   grid._grid[7][5] = 0;
   grid._grid[7][6] = 0;

   grid._grid[7][7] = 0;
   grid._grid[6][7] = 0;
   grid._grid[5][7] = 0;
   grid._grid[4][7] = 0;

   osh._orientation = 2;
   assert(grid.tryFit(osh, {7,7}));
   cout << grid << endl;

   // Test 5
   cout << "Test 5 ---------------------------------\n";
   grid.clear(9);
   osh._orientation = 3;

   grid._grid[5][0] = 0;
   grid._grid[5][1] = 0;
   grid._grid[5][2] = 0;
   grid._grid[5][3] = 0;

   grid._grid[6][3] = 0;
   grid._grid[7][3] = 0;

   assert(grid.tryFit(osh, {5,3}));
   cout << grid << endl;

   // Test 6
   cout << "Test 6 ---------------------------------\n";
   grid.clear(9);
   osh._orientation = 0;
   osh._flipped = true;

   grid._grid[0][0] = 0;
   grid._grid[1][0] = 0;
   grid._grid[2][0] = 0;
   grid._grid[3][0] = 0;

   grid._grid[3][1] = 0;
   grid._grid[3][2] = 0;

   assert(grid.tryFit(osh, {3,0}));
   cout << grid << endl;

   // Test 7
   cout << "Test 7 ---------------------------------\n";
   grid.clear(9);
   osh._orientation = 1;

   grid._grid[0][0] = 0;
   grid._grid[0][1] = 0;
   grid._grid[0][2] = 0;
   grid._grid[0][3] = 0;

   grid._grid[1][0] = 0;
   grid._grid[2][0] = 0;

   assert(grid.tryFit(osh, {0,0}));
   cout << grid << endl;

   // Test 8
   cout << "Test 8 ---------------------------------\n";
   grid.clear(9);
   osh._orientation = 2;

   grid._grid[0][0] = 0;
   grid._grid[0][1] = 0;
   grid._grid[0][2] = 0;

   grid._grid[1][2] = 0;
   grid._grid[2][2] = 0;
   grid._grid[3][2] = 0;

   assert(grid.tryFit(osh, {0,2}));
   cout << grid << endl;

   // Test 9
   cout << "Test 9 ---------------------------------\n";
   grid.clear(9);
   osh._orientation = 3;

   grid._grid[0][3] = 0;
   grid._grid[1][3] = 0;
   grid._grid[2][3] = 0;

   grid._grid[2][2] = 0;
   grid._grid[2][1] = 0;
   grid._grid[2][0] = 0;

   assert(grid.tryFit(osh, {2,3}));
   cout << grid << endl;

   // Fin
   return 0;
}
