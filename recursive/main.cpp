#include <iostream>
#include <iomanip>
#include <cassert>
#include <algorithm>
#include <chrono>

#include <cstdlib>

#include "grid.h"

bool operator<(Shape a, Shape b) {
   return a._blanks.size() < b._blanks.size();
}

int main()
{
   using namespace std::chrono;

   Grid<8,8> grid;
   PRINT_FREQ = 500ms;

   // Create Shapes
   Shape orange(4, 2);
   Shape blue(4, 3, {{0,1}, {0,2}, {3,1}, {3,2}});
   Shape red(2, 5, {{1,0}, {1,1}});
   Shape yellow(4, 3, {{2,0}, {3,0}, {2,2}, {3,2}});
   Shape purple(3, 4, {{0,2}, {0,3}, {1,2}, {1,3}});
   Shape dred(3, 3, {{0,2}});
   Shape brown(3, 4, {{2,0}, {2,1}, {0,2}, {0,3}});
   Shape green(4, 3, {{1,1}, {1,2}, {2,1}, {2,2}});

   // Assign Color to Shape using ANSI escape sequence
   bool fgcolor{true},
        bgcolor{true};
   if(fgcolor)
   {
      Grid<8,8>::COLOR_[orange._id] += "\x1b[37m";  //ANSI white
      Grid<8,8>::COLOR_[blue._id]   += "\x1b[34m";  //ANSI blue
      Grid<8,8>::COLOR_[red._id]    += "\x1b[96m";  //ANSI cyan
      Grid<8,8>::COLOR_[yellow._id] += "\x1b[33m";  //ANSI yellow
      Grid<8,8>::COLOR_[purple._id] += "\x1b[35m";  //ANSI magenta
      Grid<8,8>::COLOR_[dred._id]   += "\x1b[31m";  //ANSI red
      Grid<8,8>::COLOR_[brown._id]  += "\x1b[30m";  //ANSI black
      Grid<8,8>::COLOR_[green._id]  += "\x1b[32m";  //ANSI green
   }
   if(bgcolor)
   {
      Grid<8,8>::COLOR_[orange._id] += "\x1b[47m";  //ANSI white
      Grid<8,8>::COLOR_[blue._id]   += "\x1b[44m";  //ANSI blue
      Grid<8,8>::COLOR_[red._id]    += "\x1b[106m"; //ANSI cyan
      Grid<8,8>::COLOR_[yellow._id] += "\x1b[103m"; //ANSI yellow
      Grid<8,8>::COLOR_[purple._id] += "\x1b[45m";  //ANSI magenta
      Grid<8,8>::COLOR_[dred._id]   += "\x1b[41m";  //ANSI red
      Grid<8,8>::COLOR_[brown._id]  += "\x1b[40m";  //ANSI black
      Grid<8,8>::COLOR_[green._id]  += "\x1b[42m";  //ANSI green
   }

   // Add shapes to vector in optimal order
   std::vector<Shape> shapes = { orange
                               , blue
                               , red
                               , yellow
                               , dred
                               , green
                               , purple
                               , brown
                               };

   // Shuffle order -- comment this out to get instant solution
   //srand(time(NULL));
   srand(duration_cast<milliseconds>(steady_clock::now().time_since_epoch()).count());
   std::random_shuffle(shapes.begin(), shapes.end()); // shuffle order


   // Time Run
   auto start = steady_clock::now();
   assert(grid.makeFit({shapes.cbegin(), shapes.cend()}));
   auto time = steady_clock::now() - start;

   // Print Solution
   std::cout << grid << std::endl;

   // Print Time
   auto min = duration_cast<minutes>(time);
   double ms = duration_cast<milliseconds>(time - min).count() / 1000.0f;
   if(min.count() > 0) std::cout << min.count() << " min ";
   std::cout << ms << " sec\n";

   std::cout << "REGEX_MS:";
   std::cout << std::setw(10) << std::setfill('0');
   std::cout << duration_cast<milliseconds>(time).count() << std::endl;


   return 0;
}
