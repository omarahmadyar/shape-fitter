#ifndef __TYPES_H__
#define __TYPES_H__
#include <unordered_set>

template<typename T1, typename T2>
struct PairHash;

using coord_type = int16_t; //needs to be signed type
using coords = std::pair<coord_type, coord_type>;
using coords_hash = PairHash<coord_type, coord_type>;
using unset = std::unordered_set<coords, coords_hash>;
using shape_id = uint8_t;


#endif

