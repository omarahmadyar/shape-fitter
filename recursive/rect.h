#ifndef __RECT_H__
#define __RECT_H__
#include <functional>
#include <climits>
#include <cstring>
#include <queue>

#include "types.h"


/*
   Returns the biggest rectangular area in a 2d array
*/
template<coord_type _X, coord_type _Y>
std::size_t biggestArea(shape_id const (&arr)[_X][_Y], shape_id search = 0)
{
   /* rect stores the bottom left corner coordinate, and top right corner's height

      rectangle w/ corners {0,0}, {0,5}, {8,5}, {8,0} would be represented as {{0,0}, 5}
      and the x coord of the right corners will be found in whichever slice number the
      rect is in */
   using rect = std::pair<coords, coord_type>;
   using rects = std::vector<rect>;
   std::vector<rects> slices;
   slices.reserve(_X);

   for(coord_type x = 0; x < _X; ++x)
   {
      slices.emplace_back();
      coord_type val = -1;
      for(coord_type y = 0; y < _Y; ++y)
      {
         // Create and Update Rectangles
         if(arr[x][y] == search)
         {
            if(val < 0) val = y;

            // Create New Rectangles
            for(coord_type c = val; c <= y; ++c)
               slices.back().emplace_back(coords{x,val}, c);

            // Update Existing Rectangles
            if(slices.size() < 2) continue;
            auto &prev_column = slices.at(slices.size() - 2);
            for(auto iter = prev_column.begin(); iter != prev_column.end();)
            {
               if(iter->second == y and iter->first.second >= val)
               {
                  slices.back().push_back(*iter);
                  iter = prev_column.erase(iter);
               } else ++iter;
            }
         }
         else val = -1; // Invalidate val
      }
   }

   std::size_t biggest_area = 0;
   for(decltype(slices.size()) x = 0; x < slices.size(); ++x)
   {
      for(auto const &rect : slices[x])
      {
         int area = (rect.second - rect.first.second + 1) * (x - rect.first.first + 1);
         if(biggest_area < area) biggest_area = area;
      }
   }
   return biggest_area;
}


template<typename T, coord_type _X, coord_type _Y>
unset nonRectAreaHelper(T const (&arr)[_X][_Y], coords const &pos, T search)
{
   /*
      Helper function that performs a breadth first expansion
   */
   std::unordered_set<coords, coords_hash> explored;
   std::queue<coords> bfs;
   if(arr[pos.first][pos.second] == search) {
      bfs.push(pos);
      explored.insert(pos);
   }

   // BFS outwards exploring all adjacent locations (non diagonal)
   while(not bfs.empty())
   {
      auto const & node = bfs.front();
      for(int8_t off = -1; off <= 1; off += 2)
      {
         if(node.first + off >= 0 and node.first + off < _X
            and arr[node.first+off][node.second] == search and
            not explored.contains(coords{node.first+off,node.second}))
         {
            bfs.push(coords{node.first+off,node.second});
            explored.insert(coords{node.first+off,node.second});
         }
         if(node.second + off >= 0 and node.second + off < _Y
            and arr[node.first][node.second+off] == search and
            not explored.contains(coords{node.first,node.second+off}))
         {
            bfs.push(coords{node.first,node.second+off});
            explored.insert(coords{node.first,node.second+off});
         }

      }
      bfs.pop();
   }

   return explored;
}


/*
   Returns the target contiguous area of a 2d array

   Target: Depends on the provided comparator (default = smallest)
   Search: Whatever you are looking for (default = 0)

   Default: Will return the smallest contiguous area of 0's
   NOTE: Any continuous chain or area of search, not just rectangular.
   0 1 1 <-- will yield a result of 6     0 1 0
   0 1 0     will yield a result of 2 --> 0 1 0
   0 0 0                                  0 0 1

   Change behavior from smallest to biggest by changing OP to std::greater<T>
   Change search behavior by changing search from 0 to whatever
*/
template<typename T, typename OP = std::less<T>, coord_type _X, coord_type _Y>
std::size_t nonRectArea(T const (&arr)[_X][_Y], T search = 0, OP const &op = OP())
{
   unset undiscovered;
   for(coord_type x = 0; x < _X; ++x)
      for(coord_type y = 0; y < _Y; ++y)
         undiscovered.insert(coords{x,y});

   // Run Until All Nodes Have Been Discovered
   std::size_t target{};
   bool init{false};
   decltype(undiscovered.cbegin()) iter;
   while((iter = undiscovered.cbegin()) != undiscovered.cend())
   {
      auto explored = nonRectAreaHelper(arr, *iter, search);
      undiscovered.erase(iter);
      if(explored.size() == 0) continue;
      if(not init or op(explored.size(), target))
         target = explored.size();
      for(auto const & pos : explored)
         undiscovered.erase(pos);
      init = true;
   }
   return target;
}


#endif

